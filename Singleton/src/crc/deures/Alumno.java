package crc.deures;

public class Alumno {
	private int idAlumno; //default value 0
	private String nombre; 
	private String apellidos;
	private String dni;
	
	public int getIdAlumno() {
		return idAlumno;
	}
	public void setIdAlumno(int idAlumno) {
		this.idAlumno = idAlumno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getApellidosMaj() {
		return apellidos.toUpperCase();
	}
	public String getNombreMaj() {
		return nombre.toUpperCase();
	}

	public Alumno(int idAlumno, String nombre, String apellidos, String dni){
		this.idAlumno=idAlumno;
		this.nombre=nombre;
		this.apellidos=apellidos;
		this.dni=dni;
	}
	public Alumno(int IdAlumno){
		this.idAlumno=IdAlumno;
		this.nombre="";
		this.apellidos="";
		this.dni="";
	}
	public Alumno(){
		this.idAlumno=0;
		this.nombre="";
		this.apellidos="";
		this.dni="";
	}
	
	@Override // es meramente informativo
	public String toString() {
		return Integer.toString(idAlumno) + "," + 
				nombre + "," + 
				apellidos + "," +
				dni;
	}

}
