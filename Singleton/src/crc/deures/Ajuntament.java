package crc.deures;

//Import log4j classes.
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Ajuntament {
	
	private static final Logger log = LogManager.getLogger("ftxAjuntament");
	
	public static void main(String[] args) {
		// 
		Poblacio poble = Poblacio.getPoble();
		log.debug(poble.toString());
		poble.mostrarPoble();
		poble.unaDefuncio();
		log.debug("hem fet una defuncio");
		log.debug(poble.toString());
		
		Poblacio poble2 = Poblacio.getPoble();
		log.debug("hem creat un nou pooble2");
		log.debug(poble2.toString());
		System.out.println(poble2.mostrarPoble());
		
		poble.unNaixement();
		log.debug("hem fet un naix a poble");
		log.debug(poble.toString());
		
		System.out.println(poble2.mostrarPoble());
		poble2.unaDefuncio();
		log.debug("hem fet una defuncio a poble2");
		log.debug(poble2.toString());
		
		poble.unaDefuncio();
		log.debug("hem fet una defuncio a poble");
		log.debug(poble.toString());
		
		System.out.println(poble.mostrarPoble());
		
		
		//per demostrar que apunten al mateix objecte poble
		if (poble==poble2) System.out.println("son Iguals");

	}

}
