package crc.deures;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HashMapExemple {
	
	private static final Logger log = LogManager.getLogger("ftxHashMap");
	
	// anirem llegint els alumnes del fitxer de texte i els guardarem en un HashMap

	static HashMap<Integer, Alumno> llistaAlumnes;
	
	public static void main(String[] args){
		llistaAlumnes= new HashMap<Integer, Alumno>();
		String linia=null;
		String[] camps=new String[4];
		Alumno alumno=new Alumno();
		Alumno alumnoXap=new Alumno();
		Integer idAlumno;
		BufferedReader br;
		
		try{
			br = new BufferedReader (new FileReader (".\\alumnos.txt"));
			
			// Llegim les linees del fitxer
			// creem l'objecte alumne i afegim a HashMap
			while((linia=br.readLine())!=null){
				camps=linia.split(", ");
				alumno.setIdAlumno(Integer.parseInt(camps[0]));
				alumno.setNombre(camps[1]);
				alumno.setApellidos(camps[2]);
				alumno.setDni(camps[3]);
				log.debug("hem llegit lalumne " + alumno.toString());
				// si el codi de l'alumne no h es llavors el guarda. (no volem alumnes repetits)
				// el necessitem Integer (objecte)
				idAlumno=Integer.valueOf(alumno.getIdAlumno());
				if (!llistaAlumnes.containsKey(idAlumno)){
					alumnoXap=llistaAlumnes.put(idAlumno, alumno);
					log.debug("hem escrit a la HashTable lalumne " + alumno.toString());
				}
					//log.warning
			}
			br.close();	
		}catch(FileNotFoundException ex){
			//ex.printStackTrace();
			System.out.println("Error, en el fitxer alumnos.txt " + ex.getMessage()); 
		}catch(IOException ex){
			//ex.printStackTrace();
			System.out.println("Error, en el fitxer alumnos.txt " + ex.getMessage()); 
		}
		
		// ara farem un recorregut per la llista que hem creat
		// aqui agafem la colleccio dels alumnes que tenim a la llista
		Iterator<Integer> iterador = llistaAlumnes.keySet().iterator();
		while(iterador.hasNext()){
		     idAlumno = iterador.next();
		     alumno=llistaAlumnes.get(idAlumno);
		     log.debug("clau " + idAlumno + " - " + alumno.toString());
		}
	}
}
