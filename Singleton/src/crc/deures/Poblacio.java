package crc.deures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// aquesta classe es SINGLETON
public class Poblacio {
	
	private static final Logger log = LogManager.getLogger("ftxPoble");
	
	private static Poblacio poble;
	
	private static String nomPoble;
	private static String alcalde;
	private static int habitants;
	
	private Poblacio(){
		// crea el poble de Mataro
		nomPoble="Mataro";
		alcalde="SrAlcalde";
		habitants=10;
		log.debug("hem entrat al constructor");
	}
	public static Poblacio getPoble(){
		if (poble == null)
		 poble = new Poblacio();
		return poble;
	}	
	public int unNaixement(){
		log.debug("unNaixement");
		return ++habitants;
	}
	public int unaDefuncio(){
		log.debug("unaDefuncio");
		return (habitants > 0?--habitants:0);
	}
	public String toString(){
		log.debug("tostring");
		return (nomPoble + ", " + Integer.toString(habitants) + ", " + alcalde);
 	}
	public String mostrarPoble(){
		return ("El poble de " + nomPoble + 
				" te " + Integer.toString(habitants) + " habitants" +
				" i el seu alcalde �s " + alcalde);
 	}
}
